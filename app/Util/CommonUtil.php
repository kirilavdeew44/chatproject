<?php
/**
 * Created by IntelliJ IDEA.
 * User: kirill
 * Date: 12.09.18
 * Time: 19:10
 */

namespace App\Util;


class CommonUtil
{
    public static function getChatGreetingMessage(int $countUserInChat) : string
    {
        return "Hi from out Chat App. Now in chat: $countUserInChat. Please, enter your nickname to register in chat";
    }
}
